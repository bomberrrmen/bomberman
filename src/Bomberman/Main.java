package Bomberman;

import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) {

        System.out.println("Hellóka Világka!");

        //////////////////////// DISPLAY/GUI THREAD //////////////////////////////////

        Thread gameGUI = new Thread( new Runnable() {
            public void run() { GameGUI.runGUI(); }
        });

        gameGUI.start();

        //////////////////////// DISPLAY/GUI THREAD //////////////////////////////////
    }
}


//TODO: Random falak problémája --> szinkronizálni kell a multiplayert és a game engine-t
//TODO: Automatikusan méretezzet magát az ablak
//TODO: system clock?
//TODO: játékos textúra
//TODO: falak generálása!