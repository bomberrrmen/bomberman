package Bomberman;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import static jdk.nashorn.internal.objects.NativeArray.lastIndexOf;
import static oracle.jrockit.jfr.events.Bits.intValue;

public class Server {

    private int Port;                       // Port of the server
    private int Port_Client = 53533;        // Default client port for broadcasting
    private String IPAddress;               // IP address of the server
    private DatagramSocket Socket_server;   // The socket of the server
    private Thread BroadcastThread;         // Thread, that broadcasts CONNECTION packets to the clients with the server IP address and port number
    private Thread ReadThread;              // Thread, that reads the inputs of the clients and distributes the information
    private boolean Listening = false;      // State variable, that indicates if the server is running
    private String ID;                      // ID of the server
    private int Client_Num;                 // The number of clients that are able to connect to the server
    private String[] Clients_IP;            // String array that contains the IP addresses of the clients
    private String[] Clients_ID;
    private int[] Clients_Port;             // Int array that contains the port number of the clients, it is useful if there is no broadcasting, so the clients has different port numbers
    private String[] Clients_Config;
    private boolean Keep_Broadcasting = true;


    // Constructor
    Server(int Port, int Client_Num, String ID)
    {
        this.Port = Port;
        this.ID = ID;
        this.Client_Num = Client_Num;
        Clients_IP = new String[Client_Num];
        Clients_ID = new String[Client_Num];
        Clients_Port = new int[Client_Num];
        Clients_Config = new String[Client_Num];

        for (int i = 0; i < Client_Num; i++)
        {
            Clients_IP[i] = "";
            Clients_Port[i] = 0;
            Clients_ID[i] = "";
            Clients_Config[i] = "";
        }
    }

    // Function, that starts the server
    public void Start()
    {
        try
        {
            // Creating a UDP server socket
            Socket_server = new DatagramSocket(Port);

            // If the socket cant receive data for 2 seconds, it will create an exception,
            // that useful in case of closing the socket, so the socket wont be stuck in data receiving and can close it safely:
            Socket_server.setSoTimeout(2000);

            // Getting the IP address:
            InetAddress iAddress = Socket_server.getInetAddress();
            DatagramSocket socket = new DatagramSocket();
            socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
            String Address = socket.getLocalAddress().getHostAddress();
            IPAddress = Address.substring(Address.indexOf('/') + 1);
            ErrorHandler("IP Address: " + IPAddress, false);
        }
        catch (SocketException e)
        {
            e.printStackTrace();
            ErrorHandler("Server cannot start!", true);
            return;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        ErrorHandler("Server started!", false);

        // Indicating that the server is running:
        Listening = true;

        // Creating the listen thread:
        BroadcastThread = new Thread(new Runnable() {
           public void run()
           {
               Broadcast();
           }
        });

        // Creating the reading thread:
        ReadThread = new Thread(new Runnable() {
            public void run()
            {
                Transmit();
            }
        });

        // Starting the threads:
        BroadcastThread.start();
        ReadThread.start();
    }


    // Broadcasting thread, that sends CONNECTION packets to all IP addresses:
    private void Broadcast()
    {

        // Create the CONNECTION packet:
        Multiplayer_Packet Connect_Packet = new Multiplayer_Packet(this.ID, "CONNECT",0, 0);

        // Serialize into a string:
        String Connect_string = Connect_Packet.Serialize();

        // Creating the broadcast IP address:
        String Broadcast_address = IPAddress.substring(0, IPAddress.lastIndexOf("."));

        InetAddress Address;

        try
        {
            //Address = InetAddress.getByName(Broadcast_address + ".255");
            Address = InetAddress.getByName("255.255.255.255");

        }
        catch (UnknownHostException e)
        {
            ErrorHandler("Broadcasting failed!", true);
            e.printStackTrace();
            return;
        }

        // Creating the UDP packet according to the broadcast address and the default client port:
        DatagramPacket Connect_DatagramPacket = null;
        try
        {
            Connect_DatagramPacket = new DatagramPacket(Connect_string.getBytes("UTF-8"), Connect_string.getBytes("UTF-8").length, Address, Port_Client);
        } catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }

        ErrorHandler("Broadcasting started!", false);


        // The thread running as long as the given number of clients are not connected:
        boolean Okay = false;
        while (Okay != true && Keep_Broadcasting)
        {
            try
            {
                // Send the packet:
                Socket_server.send(Connect_DatagramPacket);

                // Wait 100 ms between sendings:
                TimeUnit.MILLISECONDS.sleep(100);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

            // Check if the given number of clients has connected or not:
            Okay = true;
            for (int i = 0; i < Client_Num; i++)
            {
                if (Clients_IP[i] == "")
                {
                    Okay = false;
                }
            }
        }

        ErrorHandler("Broadcasting thread closed!", false);
        return;

    }



    // Reading thread, that reads the input streams of the clients then passes the information to every other clients:
    private void Transmit()
    {

        // The thread is running until the server is running:
        while (Listening)
        {

            // Creating a byte array and an UDP packet to receive data:
            byte[] Data = new byte[100];

            DatagramPacket Packet = new DatagramPacket(Data, Data.length);

            try
            {
                // Receiving data (blocking the thread for 2 seconds at max):
                Socket_server.receive(Packet);

                // If there is received data, then convert it into a string:
                String Received_Data = null;
                try {
                    Received_Data = new String(Data, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                //ErrorHandler("Packet received: " + Received_Data, false);

                // Create a packet object and derserialize the string:
                Multiplayer_Packet Data_Processed = new Multiplayer_Packet();

                Data_Processed.Deserialize(Received_Data);

                // If the input packet is a NEW_PLAYER packet:
                if (Data_Processed.getType().equals("NEW_PLAYER"))
                {
                    // Cycle through the client array the insert the new client:
                    for (int i = 0; i < Client_Num; i++)
                    {
                        if (Clients_IP[i] == "")
                        {
                            String Address = Packet.getAddress().toString();
                            Clients_IP[i] = Address.substring(1);
                            Clients_ID[i] = Data_Processed.getID();
                            Clients_Port[i] = Packet.getPort();

                            //ErrorHandler("New player added: " + Data_Processed.getID(), false);
                            break;
                        }
                    }

                    for (int i = 0; i < Client_Num; i++)
                    {
                        if (!Clients_ID[i].equals("") && !Clients_ID[i].equals(Data_Processed.getID()))
                        {
                            Multiplayer_Packet Player_Packet = new Multiplayer_Packet(Clients_ID[i], "NEW_PLAYER", 0, 0);
                            String Player_String = Player_Packet.Serialize();
                            byte[] Player_Byte = Player_String.getBytes("UTF-8");


                            Multiplayer_Packet Config_Packet = new Multiplayer_Packet(Clients_ID[i], "CONFIG", 0, 0, Clients_Config[i]);
                            String Config_String = Config_Packet.Serialize();
                            byte[] Config_Byte = Config_String.getBytes("UTF-8");

                            InetAddress Address_player = InetAddress.getByName(Packet.getAddress().toString().substring(1));

                            DatagramPacket Player = new DatagramPacket(Player_Byte, Player_Byte.length, Address_player, Packet.getPort());
                            DatagramPacket Config = new DatagramPacket(Config_Byte, Config_Byte.length, Address_player, Packet.getPort());

                            try
                            {
                                Socket_server.send(Player);
                                Socket_server.send(Config);
                            }
                            catch (Exception e)
                            {
                                ErrorHandler("New player and config messages are not sent!", true);
                            }

                        }
                    }

                }

                if (Data_Processed.getType().equals("CONFIG"))
                {
                    for (int i = 0; i < Client_Num; i++)
                    {
                        if (Clients_ID[i].equals(Data_Processed.getID()))
                        {
                            Clients_Config[i] = Data_Processed.getMessage();
                        }
                    }
                }

                // If the input packet is not a CONNECT packet, that the server broadcasts:
                if (!Data_Processed.getType().equals("CONNECT"))
                {
                    // Cycle through the clients:
                    for (int i = 0; i < Client_Num; i++) {

                        // If the client is not empty:
                        if (Clients_IP[i] != "")
                        {
                            try {

                                // Get the IP address of the client:
                                InetAddress Address = InetAddress.getByName(Clients_IP[i]);

                                // Create an UDP packet with the actual client's IP address and port number:
                                DatagramPacket Packet2Client = new DatagramPacket(Packet.getData(), Packet.getData().length, Address, Clients_Port[i]);

                                try
                                {
                                    // Send the input packet to the clients:
                                    Socket_server.send(Packet2Client);
                                    //ErrorHandler("Message transmitted!", false);

                                }
                                catch (IOException e)
                                {
                                    //ErrorHandler("Message not transmitted!", true);
                                    e.printStackTrace();
                                    return;
                                }

                            } catch (UnknownHostException e) {
                                e.printStackTrace();
                                return;
                            }


                        }
                    }
                }


                // If the input packet is a QUIT packet:
                if (Data_Processed.getType().equals("QUIT"))
                {
                    // Get the IP address of the sender:
                    String Address = Packet.getAddress().toString().substring(1);

                    // Cycle through the clients, and delete the quitting client:
                    for (int i = 0; i < Client_Num; i++)
                    {
                        if (Address.equals(Clients_IP[i]))
                        {
                            Clients_IP[i] = "";
                            Clients_ID[i] = "";
                            Clients_Port[i] = 0;

                            //ErrorHandler("Player removed: " + Data_Processed.getID(), false);
                            break;
                        }
                    }
                }
            }

            // If a timeout exception has occured:
            catch (SocketTimeoutException e)
            {
                // If the close() function has been called and it changed the status flag to false:
                if (Listening == false)
                {
                    // Call the terminate() function, that closes the socket safely:
                    Terminate();
                }
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }


        }

    }


    // Function, that stops the data receiving in the next timeout:
    public void Close()
    {
        Listening = false;
    }

    // Function, that closes the socket:
    private void Terminate()
    {
        // Close the server socket:
        Socket_server.close();

        ErrorHandler("Socket closed!", false);

    }


    // Getter function, that returns the IP address of the server:
    public String getIP()
    {
        return IPAddress;
    }


    // Getter function, that returns the port of the server:
    public int getPort()
    {
        return Port;
    }

    public int getClients()
    {
        int counter = 0;
        for (int i = 0; i < Client_Num; i++)
        {
            if (!Clients_IP[i].equals(""))
            {
                counter++;
            }
        }

        return counter;
    }


    // Diagnostic function, that print out different messages for debugging purposes:
    private void ErrorHandler(String Error, boolean isError)
    {
        // The second argument decides if it is a general text or error text:
        if (isError)
        {
            System.out.println("ERROR: " + Error + " (Server: " + ID + ")");
        }
        else
        {
            System.out.println(Error + " (Server: " + ID + ")");
        }
    }

    public void KillBroadcast()
    {
        Keep_Broadcasting = false;
    }

}
