package Bomberman;

import java.awt.*;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

import static Bomberman.GameEngine.*;

public class Client {

    private int ServerPort = 0;                 // The port of the server
    private String ServerAddress = "";          // The IP address of the server
    private InetAddress iServerAddress;         // The IP address of the server
    private boolean Listening = false;          // State variable, that indicates if the client is connected to the server
    private Thread ListenThread;                // Thread, that monitors the input stream and read the data out
    private String ID;                          // The ID of the client
    private DatagramSocket Socket;              // UDP socket for communication
    private int Client_Port = 53533;            // The default port number to clients, the server will broadcast its' IP address and port number to this port number
    private boolean Ready = false;              // Flag, that indicates if the client is able to send data, so it has the server IP address and port number
    private DatagramPacket Packet_Received;     // The Packet, that receives the information from the server


    // Constructor
    Client (String Client_ID)
    {
        ID = Client_ID;
    }


    // Function, that creates the client socket and starts the receiving thread:
    public void Connect()
    {
            try
            {
                // Creating a new client socket at the default port:
                Socket = new DatagramSocket(Client_Port);

               // ErrorHandler("IP Address: " + Socket.getLocalAddress().toString(), false);

                ErrorHandler("Socket created!", false);

            }
            catch (IOException e)
            {
                e.printStackTrace();
                ErrorHandler("Socket not created!", true);
                return;
            }

            // Indicating that the client is connected:
            Listening = true;

            // Creating the listening thread:
            ListenThread = new Thread(new Runnable() {
                public void run()
                {
                    Receive();
                }
            });

            // Starting the listening thread:
            ListenThread.start();

            // Waiting 1 second, to get the CONNECTION packet and answer to it with a NEW_PLAYER message:
            try
            {
                TimeUnit.SECONDS.sleep(1);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }

    }


    // Function, that sends data to the server:
    public void Send(Multiplayer_Packet Data)
    {
        // If the connection exists:
        if (Ready == true)
        {
            // Set the packet ID:
            Data.setID(this.ID);

            // Serialize the object to a string:
            String Data_string = Data.Serialize();

            // Create byte code from the string:
            byte[] Data_byte = null;
            try {
                Data_byte = Data_string.getBytes("UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            // Create UDP Packet from the byte code, set the server IP address and port number:
            DatagramPacket Packet = new DatagramPacket(Data_byte, Data_byte.length, iServerAddress, ServerPort);

            try {
                // Sending the data to the server:
                Socket.send(Packet);
                //ErrorHandler("Data sent!", false);
            } catch (Exception e) {
                e.printStackTrace();
                ErrorHandler("Data not sent!", true);
            }
        }
    }


    // Listening thread, that waits for input data:
    public void Receive()
    {
        // The listening is running until the client is connected:
        while (Listening)
        {
            // Create a byte array that contains the received data:
            byte[] Data = new byte[100];

            // Create an UDP packet for receiving:
            Packet_Received = new DatagramPacket(Data, Data.length);

            try
            {
                // Receive input data, it will block the thread until there is data:
                Socket.receive(Packet_Received);

                // Create a string from the received byte code:
                String Received_Data = null;
                try {
                    Received_Data = new String(Data, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                //ErrorHandler("Packet received!", false);

                // Process the input data:
                Process(Received_Data);

            }
            catch (IOException e)
            {
                e.printStackTrace();
                ErrorHandler("Received packet error!", true);
                return;
            }


        }

    }


    // Function, that processes the received data:
    private void Process(String Data)
    {
        // Create a data object:
        Multiplayer_Packet Received_Data = new Multiplayer_Packet();


        // Deserialize the input string into the object:
        Received_Data.Deserialize(Data);

        if (!Received_Data.getID().equals(this.ID) && !Received_Data.getType().equals("CONNECT"))
        {
            ErrorHandler("Received Data: ID: " + Received_Data.getID() + /*", IP: " + Received_Data.getIP() +*/ ", Type: " + Received_Data.getType() + ", X: " + Received_Data.getX() + ", Y: " + Received_Data.getY() + ", Message: " + Received_Data.getMessage(), false);
        }

        // Select by the data type:
        switch(Received_Data.getType())
        {

            // If it is a broadcast message from the server:
            case "CONNECT":
            {
                // If no CONNECT data has arrived yet:
                if (Ready == false)
                {
                    // Get the server IP adress:
                    ServerAddress = Packet_Received.getAddress().toString().substring(1);
                    try {
                        this.iServerAddress = InetAddress.getByName(ServerAddress);
                    } catch (UnknownHostException e) {
                        e.printStackTrace();
                        ErrorHandler("Address not valid! (Client)", true);
                    }

                    // Get the server port number:
                    ServerPort = Packet_Received.getPort();

                    // Indicate, that the connection has established:
                    Ready = true;
                    ErrorHandler("Received IP information from server!", false);

                    // Create a NEW_PLAYER answer message:
                    Multiplayer_Packet Connect_Data = new Multiplayer_Packet("", "NEW_PLAYER", 0, 0);

                    try {
                        // Send it to the server:
                        Send(Connect_Data);
                    }
                    catch (Exception e){
                        ErrorHandler("New player packet not sent!", true);
                    }
                }


                break;
            }

            // If a new player has joined:
            case "NEW_PLAYER":
            {
                // Add new player to the game...
                if (null != GameEngine.findPlayerById(Received_Data.getID()))
                    return;

                if (!Received_Data.getID().equals(this.ID)) {
                    remoteNewPlayer(Received_Data.getID(), Received_Data.getX(), Received_Data.getY());
                    Player newPlayer = GameEngine.findPlayerById(Received_Data.getID());
                    newPlayer.initPosOnMap = "A";
                    newPlayer.playerColor = Color.BLUE;
                    newPlayer.ready = false;

                    ErrorHandler("Player: " + Received_Data.getID() + " added to the game!", false);
                }
                break;
            }

            // If a player has moved:
            case "MOVEMENT":
            {
                // Refresh the given player's position....

                if (!Received_Data.getID().equals(this.ID))
                {
                    int X = Math.round(Received_Data.getX());
                    remoteMovement(Received_Data.getID(), Received_Data.getX(), Received_Data.getY(), Integer.parseInt(Received_Data.getMessage()));

                    ErrorHandler("Player: " + Received_Data.getID() + "'s position has changed!", false);
                }
                break;
            }

            // If a bomb has been placed:
            case "BOMB":
            {
                // Put down a bomb objective...

                if (!Received_Data.getID().equals(this.ID)) {
                    remoteBombPlacement(Received_Data.getID(), Received_Data.getX(), Received_Data.getY());

                    ErrorHandler("Player: " + Received_Data.getID() + " has dropped a bomb!", false);
                }
                break;
            }

            // If a player has quited:
            case "QUIT":
            {
                // Remove player...

                // If the player is the client itself, then indicate the end of the connection:
                if (Received_Data.getID().equals(this.ID))
                {
                    Listening = false;
                }
                else
                {
                    remoteQuit(Received_Data.getID());
                    ErrorHandler("Player: " + Received_Data.getID() + " has left the game.", false);
                }

                break;
            }

            case "WALL":
            {
                if (!Received_Data.getID().equals(this.ID))
                {
                    remoteWallPlacement(Received_Data.getX(), Received_Data.getY(), Received_Data.getMessage());
                    ErrorHandler("Wall has been added to the game!", false);
                }

                break;
            }

            case "CONFIG":
            {
                // Adjust configuration...

                if (Received_Data.getID().equals(this.ID))
                    return;

                if (Received_Data.getMessage().equals(""))
                    return;

                Player actualPlayer = GameEngine.findPlayerById(Received_Data.getID());
                if (actualPlayer == null) {
                    remoteNewPlayer(Received_Data.getID(), Received_Data.getX(), Received_Data.getY());
                    actualPlayer = GameEngine.findPlayerById(Received_Data.getID());
                }
                String[] configSettings = Received_Data.getMessage().split("\\|");

                actualPlayer.initPosOnMap = configSettings[0];

                switch (configSettings[1]) {
                    case "Blue":
                        actualPlayer.playerColor = Color.BLUE;
                        break;
                    case "Red":
                        actualPlayer.playerColor = Color.RED;
                        break;
                    case "Green":
                        actualPlayer.playerColor = Color.GREEN;
                        break;
                    case "Pink":
                        actualPlayer.playerColor = Color.MAGENTA;
                        break;
                    default:
                        actualPlayer.playerColor = Color.GRAY;
                }

                actualPlayer.ready = configSettings[2].equals("true");

                GameGUI.setMapSize(configSettings[3]);

                ErrorHandler("Configuration has been adjusted!", false);
                break;
            }

            case "START":
            {

                if (!Received_Data.getID().equals(this.ID))
                {

                    GameGUI.remoteStartGame();
                    ErrorHandler("The game has started!", false);
                }
                break;
            }

            default:
            {
                // Invalid packet...

                ErrorHandler("Invalid packet from: " + Received_Data.getID(), true);
                break;
            }
        }

    }

    // Get the ID of the client:
    public String getID()
    {
        return this.ID;
    }


    // Function, that closes the connection and socket:
    public void Close()
    {
        // Create a QUIT message for the server:
        Multiplayer_Packet Quit_Data = new Multiplayer_Packet("", "QUIT",0, 0);

        // Send it:
        Send(Quit_Data);

        // Wait 1 second, giving time to the receiving thread to process the QUIT data form the server,
        // and it will toggle the listening flag to false, so the listening thread will terminate and it is safe to close the socket:
        try
        {
            TimeUnit.SECONDS.sleep(1);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        // Indicating that the client connection is no longer alive:
        Listening = false;

        // Closing the client socket:
        Socket.close();
        ErrorHandler("Socket closed!", false);


    }

    // Get the state of the connection:
    public boolean isReady()
    {
        return Ready;
    }

    // Diagnostic function, that prints out debugging informations:
    private void ErrorHandler(String Error, boolean isError)
    {
        // The second argument decides if is is an simple text or an error text:
        if (isError)
        {
            System.out.println("ERROR: " + Error + " (Client: " + ID + ")");
        }
        else
        {
            System.out.println(Error + " (Client: " + ID + ")");
        }
    }
}
