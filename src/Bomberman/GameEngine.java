package Bomberman;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import static Bomberman.GameGUI.getGameStart;
import static java.lang.Math.abs;

public class GameEngine {

    private static int cellSize;
    private static int cellNumberX, cellNumberY;
    private static int margin;
    private static int Reward_Probability = 25; // 0..100
    private static int Wall_Probability = 80;
    private static boolean Walls_Sent = false;

    /* Objects on the field / map */
    public static String localPlayersName;
    private static List<Player> players = new LinkedList<>();
    private static List<Wall> walls = new LinkedList<>();
    private static List<Bomb> bombs = new LinkedList<>();
    private static List<Reward> rewards = new LinkedList<>();
    public static volatile boolean initDone = false;  // For synchronization

    enum KEY_COMMANDS { MOVE_LEFT, MOVE_RIGHT, MOVE_UP, MOVE_DOWN, PLACE_BOMB }

    public static void initPlayersList() {
        /* Synchronization with the multiplayer */
        while (true) {
            if (Multiplayer.initDone)
                break;
        }
        localPlayersName = Multiplayer.getClientID();
        players.add(new Player(localPlayersName));
    }

    public static void runEngine(int inputCellSize, int inputCellNumberX, int inputCellNumberY, int inputMargin) {

        cellSize = inputCellSize;
        cellNumberX = inputCellNumberX;
        cellNumberY = inputCellNumberY;
        margin = inputMargin;

        /* Instantiating objects */
        /* Solid walls */
        for (int idx = 0; idx < (2*cellNumberY + 2*cellNumberX - 4); idx++) {  // The circumference of the playing field
            walls.add(new Wall(false));
        }

        for (int i = 3; i < cellNumberY; i = i + 2)
        {
            for (int j = 3; j < cellNumberX; j = j + 2)
            {
                walls.add(new Wall(false, j * 60, i * 60));
            }
        }

        // The server creates the explodable walls:
        if (Multiplayer.getIsServer())
        {
            Random Random_Selector = new Random();
            int alma;

            boolean simple = true;
            for (int i = 2; i < cellNumberY; i++)
            {
                for (int j = 2; j < cellNumberX; j++)
                {
                    if (simple == true)
                    {
                        alma = Random_Selector.nextInt(100);
                        if (alma <= Wall_Probability)
                        {
                            walls.add(new Wall(true, j * 60, i * 60));
                        }
                    }
                    else
                    {
                        if ((j % 2) == 0)
                        {
                            alma = Random_Selector.nextInt(100);
                            if (alma <= Wall_Probability)
                            {
                                walls.add(new Wall(true, j * 60, i * 60));
                            }
                        }
                    }

                    if (j == (cellNumberX - 1))
                    {
                        simple = (simple == true)?false:true;
                    }
                }
            }


            Iterator<Wall> wallIterator = walls.iterator();
            while (wallIterator.hasNext()) {

                Wall actualWall = wallIterator.next();

                if (actualWall.explodable == true) {
                    int x = actualWall.getxPos();
                    int y = actualWall.getyPos();


                    if ((x == 120 && y == 120) || (x == 180 && y == 120) || (x == 120 && y == 180)) {
                        wallIterator.remove();
                    } else if ((x == (cellNumberX - 1)*60 && y == 120) || (x == (cellNumberX - 2)*60 && y == 120) || (x == (cellNumberX - 1)*60 && y == 180)) {
                        wallIterator.remove();
                    } else if ((x == 120 && y == (cellNumberY - 1)*60) || (x == 180 && y == (cellNumberY - 1)*60) || (x == 120 && y == (cellNumberY - 2)*60)) {
                        wallIterator.remove();
                    } else if ((x == (cellNumberX - 1)*60 && y == (cellNumberY - 1)*60) || (x == (cellNumberX - 2)*60 && y == (cellNumberY - 1)*60) || (x == (cellNumberX - 1)*60 && y == (cellNumberY - 2)*60)) {
                        wallIterator.remove();
                    }


                }
            }


        }


        /* Giving initial information to painted objects */
        /* Players */
        int xInitPosOfLocalPlayer = 0;
        int yInitPosOfLocalPlayer = 0;

        for (Player actualPlayer : players) {
            switch (actualPlayer.initPosOnMap) {
                case "A":
                    xInitPosOfLocalPlayer = margin + cellSize + cellSize / 2;
                    yInitPosOfLocalPlayer = margin + cellSize + cellSize / 2;
                    break;
                case "B":
                    xInitPosOfLocalPlayer = margin + cellSize + cellSize / 2;
                    yInitPosOfLocalPlayer = margin + cellSize + cellSize / 2 + (cellNumberY - 3) * cellSize;
                    break;
                case "C":
                    xInitPosOfLocalPlayer = margin + cellSize + cellSize / 2 + (cellNumberX - 3) * cellSize;
                    yInitPosOfLocalPlayer = margin + cellSize + cellSize / 2 + (cellNumberY - 3) * cellSize;
                    break;
                case "D":
                    xInitPosOfLocalPlayer = margin + cellSize + cellSize / 2 + (cellNumberX - 3) * cellSize;
                    yInitPosOfLocalPlayer = margin + cellSize + cellSize / 2;
                    break;
            }
            actualPlayer.setxPos(xInitPosOfLocalPlayer);
            actualPlayer.setyPos(yInitPosOfLocalPlayer);
        }

        /* Solid walls */
        for (int idx = 0; idx < (cellNumberY-1); idx++) {    // cellNumberY-1 blocks per vertical edge

            /* Left edge */
            walls.get(2 * idx).setxPos(margin + cellSize / 2);
            walls.get(2 * idx).setyPos(margin + cellSize / 2 + idx * cellSize);

            /* Right edge */
            walls.get(2 * idx + 1).setxPos(margin + cellSize / 2 + (cellNumberX - 1) * cellSize);
            walls.get(2 * idx + 1).setyPos(margin + cellSize / 2 + cellSize + idx * cellSize);
        }

        int idxOffset = 2*(cellNumberY-1);
        for (int idx = 0; idx < (cellNumberX-1); idx++) {    // cellNumberX-1 blocks per horizontal edge

            /* Upper edge */
            try {
                walls.get(idxOffset + 2 * idx).setxPos(margin + cellSize / 2 + cellSize + idx * cellSize);
                walls.get(idxOffset + 2 * idx).setyPos(margin + cellSize / 2);
            } catch (Exception e) {
                System.out.println("X index = " + idx);
            }

            /* Bottom edge */
            walls.get(idxOffset + 2 * idx + 1).setxPos(margin + cellSize/2 + idx*cellSize);
            walls.get(idxOffset + 2 * idx + 1).setyPos(margin + cellSize/2 + (cellNumberY-1)*cellSize);
        }

        initDone = true;
    }

    public static List<Player> gimmeMyPlayers() {

        //String text = "length is zero";
        //if (players.size() != 0) text = "length is not zero";
        //System.out.println("Returning players: " + text);
        return players;
    }

    public static List<Wall> gimmeMyWalls() {

        String text = "length is zero";
        if (walls.size() != 0) text = "length is not zero";
        //System.out.println("Returning walls: " + text);
        return walls;
    }

    public static List<Bomb> gimmeMyBombs() {

        String text = "length is zero";
        if (bombs.size() != 0) text = "length is not zero";
        //System.out.println("Returning bombs: " + text);
        return bombs;
    }

    public static List<Reward> gimmeMyRewards() {
        return rewards;
    }

    public static void processKeyPressed(KEY_COMMANDS keyCommand) {

        Player localPlayer = findPlayerById(localPlayersName);
        if (localPlayer != null) {

            switch (keyCommand) {
                case MOVE_LEFT:
                    if (cellIsEmpty(localPlayer.getxPos(false) + (-1) * cellSize,
                                    localPlayer.getyPos(false) + 0) == 0) {
                        localPlayer.setxStep((-1) * cellSize);
                    }
                    break;

                case MOVE_RIGHT:
                    if (cellIsEmpty(localPlayer.getxPos(false) + cellSize,
                                    localPlayer.getyPos(false) + 0) == 0) {
                        localPlayer.setxStep(cellSize);
                    }
                    break;

                case MOVE_UP:
                    if (cellIsEmpty(localPlayer.getxPos(false) + 0,
                                    localPlayer.getyPos(false) + (-1) * cellSize) == 0) {
                        localPlayer.setyStep((-1) * cellSize);
                    }
                    break;

                case MOVE_DOWN:
                    if (cellIsEmpty(localPlayer.getxPos(false) + 0,
                                    localPlayer.getyPos(false) + cellSize) == 0) {
                        localPlayer.setyStep(cellSize);
                    }
                    break;
                case PLACE_BOMB:
                    int x = localPlayer.getxPos(true);
                    int y = localPlayer.getyPos(true);

                    if (localPlayer.getActualBomb() < localPlayer.getMaxBomb()) {

                        localPlayer.increaseActualBomb();
                        bombs.add(
                                new Bomb(x,
                                        y,
                                        localPlayer.getBombRange(),
                                        localPlayer.id,
                                        localPlayer.getBombTime()
                                )
                        );
                        Multiplayer_Packet bombPlacementPacket = new Multiplayer_Packet("BOMB");
                        bombPlacementPacket.setXY(x, y);
                        Multiplayer.Send(bombPlacementPacket);
                    }
                    break;
            }
        }
    }

    private static int cellIsEmpty(int x, int y) {

        int objectCode = 0;  // 0 - Empty cell

        for (Player actualPlayer : players) {
            boolean xIsTheSame = (abs(x - actualPlayer.getxPos(false)) < cellSize/2);
            boolean yIsTheSame = (abs(y - actualPlayer.getyPos(false)) < cellSize/2);
            if (xIsTheSame && yIsTheSame) {
                objectCode = 1;  // 1 - Player
                break;
            }
        }

        for (Wall actualWall : walls) {
            boolean xIsTheSame = (abs(x - actualWall.getxPos()) < cellSize/2);
            boolean yIsTheSame = (abs(y - actualWall.getyPos()) < cellSize/2);
            if (xIsTheSame && yIsTheSame) {
                objectCode = 2;  // 2 - Wall
                break;
            }
        }

        for (Bomb actualBomb : bombs) {
            boolean xIsTheSame = (abs(x - actualBomb.getxPos()) < cellSize/2);
            boolean yIsTheSame = (abs(y - actualBomb.getyPos()) < cellSize/2);
            if (xIsTheSame && yIsTheSame && objectCode != 1) {
                objectCode = 3;  // 3 - Bomb
                break;
            }
        }

        return objectCode;
    }

    private static boolean wallInCellIsExplodable(int x, int y) {

        boolean explodable = false;

        for (Wall actualWall : walls) {
            boolean xIsTheSame = (abs(x - actualWall.getxPos()) < cellSize/2);
            boolean yIsTheSame = (abs(y - actualWall.getyPos()) < cellSize/2);
            if (xIsTheSame && yIsTheSame) {
                if (actualWall.explodable) explodable = true;
                break;
            }
        }

        return explodable;
    }

    private static void deleteWall(int x, int y) {

        Iterator<Wall> wallIterator = walls.iterator();
        while (wallIterator.hasNext()) {

            Wall actualWall = wallIterator.next();

            boolean xIsTheSame = (abs(x - actualWall.getxPos()) < cellSize/2);
            boolean yIsTheSame = (abs(y - actualWall.getyPos()) < cellSize/2);
            if (xIsTheSame && yIsTheSame) {
                wallIterator.remove();

                if (!actualWall.getReward().equals(""))
                rewards.add(new Reward(actualWall.getReward(), actualWall.getxPos(), actualWall.getyPos()));

                break;
            }
        }
    }

    public static void processKeyReleased(KEY_COMMANDS keyCommand) {

        if ((keyCommand == KEY_COMMANDS.MOVE_LEFT) || (keyCommand == KEY_COMMANDS.MOVE_RIGHT)) {
            Player actualPlayer = findPlayerById(localPlayersName);
            if (actualPlayer != null)
                actualPlayer.setxStep(0);
        }
        if ((keyCommand == KEY_COMMANDS.MOVE_UP) || (keyCommand == KEY_COMMANDS.MOVE_DOWN)) {
            Player actualPlayer = findPlayerById(localPlayersName);
            if (actualPlayer != null)
                actualPlayer.setyStep(0);
        }
    }

    public static void updateGameState() {

        for (Player actualPlayer : players) {
            actualPlayer.updatePos();

            int Reward_Index = findRewardByPosition(actualPlayer.getActualxPos(), actualPlayer.getActualyPos());
            if (Reward_Index != -1)
            {
                switch (rewards.get(Reward_Index).getType())
                {
                    case "SPEED":
                    {
                        if (actualPlayer.getID().equals(localPlayersName))
                        {
                            actualPlayer.increaseSpeed();
                        }
                        break;
                    }
                    case "BOMB":
                    {
                        if (actualPlayer.getID().equals(localPlayersName))
                        {
                            actualPlayer.increaseMaxBomb();
                        }
                        break;
                    }
                    case "RANGE":
                    {
                        if (actualPlayer.getBombRange().equals(Bomb.RANGE.DEFAULT_RANGE))
                        {
                            actualPlayer.setBombRange(Bomb.RANGE.ENHANCED_RANGE);
                        }
                        else if(actualPlayer.getBombRange().equals(Bomb.RANGE.ENHANCED_RANGE))
                        {
                            actualPlayer.setBombRange(Bomb.RANGE.SUPER_RANGE);
                        }
                        else if (actualPlayer.getBombRange().equals(Bomb.RANGE.SUPER_RANGE))
                        {
                            actualPlayer.setBombRange(Bomb.RANGE.HOLYSHIT_RANGE);
                        }
                        break;
                    }
                    case "HP":
                    {
                        if (actualPlayer.getID().equals(localPlayersName)) {
                            actualPlayer.increaseHP();
                        }
                        break;
                    }
                    case "TIME":
                    {
                        actualPlayer.decreaseBombTime();
                        break;
                    }
                    default:
                    {
                        break;
                    }
                }
                rewards.remove(Reward_Index);
            }
        }

        Iterator<Bomb> bombIterator = bombs.iterator();
        while (bombIterator.hasNext()) {
            Bomb actualBomb = bombIterator.next();
            boolean actualBombJustExploded = actualBomb.update();

            if (actualBombJustExploded) {
                int x0 = actualBomb.getxPos();
                int y0 = actualBomb.getyPos();
                int range = actualBomb.getRangeValue();
                int objectCode;

                Player localPlayer = findPlayerById(localPlayersName);
                if (localPlayer == null) {
                    System.out.println("Bomb explosion. Local player not found by its ID!");
                }

                if (cellIsEmpty(x0, y0) == 1 && (findPlayerByPosition(x0, y0).equals(localPlayer))) {

                    localPlayer.decreaseHP();

                    if (localPlayer.getHP() == 0)
                    {
                        GameGUI.gameOver();
                        Multiplayer_Packet quitPacket = new Multiplayer_Packet("QUIT");
                        Multiplayer.Send(quitPacket);
                    }
                }

                for (int x = (x0 - cellSize); x >= (x0 - range*cellSize); x -= cellSize ) {

                    objectCode = cellIsEmpty(x, y0);

                    if (objectCode != 0) {

                        if (objectCode == 1 && (findPlayerByPosition(x, y0).equals(localPlayer))) {
                            actualBomb.setFlameAreaLeft(x);
                            findPlayerById(localPlayersName).decreaseHP();
                            if (findPlayerById(localPlayersName).getHP() == 0)
                            {
                                GameGUI.gameOver();
                                Multiplayer_Packet quitPacket = new Multiplayer_Packet("QUIT");
                                Multiplayer.Send(quitPacket);
                            }
                            break;
                        } else if (objectCode == 2 && wallInCellIsExplodable(x, y0)) {
                            actualBomb.setFlameAreaLeft(x);
                            deleteWall(x, y0);
                            break;
                        } else {
                            actualBomb.setFlameAreaLeft(x + cellSize);
                            break;
                        }
                    }

                    if (x == (x0 - range*cellSize))
                        actualBomb.setFlameAreaLeft(x);
                }

                for (int x = (x0 + cellSize); x <= (x0 + range*cellSize); x += cellSize ) {

                    objectCode = cellIsEmpty(x, y0);

                    if (objectCode != 0 ) {

                        if (objectCode == 1 && findPlayerByPosition(x, y0).equals(localPlayer)) {
                            actualBomb.setFlameAreaRight(x);
                            findPlayerById(localPlayersName).decreaseHP();
                            if (findPlayerById(localPlayersName).getHP() == 0)
                            {
                                GameGUI.gameOver();
                                Multiplayer_Packet quitPacket = new Multiplayer_Packet("QUIT");
                                Multiplayer.Send(quitPacket);
                            }
                            break;
                        } else if (objectCode == 2 && wallInCellIsExplodable(x, y0)) {
                            actualBomb.setFlameAreaRight(x);
                            deleteWall(x, y0);
                            break;
                        } else {
                            actualBomb.setFlameAreaRight(x - cellSize);
                            break;
                        }
                    }

                    if (x == (x0 + range*cellSize))
                        actualBomb.setFlameAreaRight(x);
                }

                for (int y = (y0 - cellSize); y >= (y0 - range*cellSize); y -= cellSize ) {

                    objectCode = cellIsEmpty(x0, y);

                    if (objectCode != 0 ) {

                        if (objectCode == 1 && findPlayerByPosition(x0, y).equals(localPlayer)) {
                            actualBomb.setFlameAreaUp(y);
                            findPlayerById(localPlayersName).decreaseHP();
                            if (findPlayerById(localPlayersName).getHP() == 0)
                            {
                                GameGUI.gameOver();
                                Multiplayer_Packet quitPacket = new Multiplayer_Packet("QUIT");
                                Multiplayer.Send(quitPacket);
                            }
                            break;
                        } else if (objectCode == 2 && wallInCellIsExplodable(x0, y)) {
                            actualBomb.setFlameAreaUp(y);
                            deleteWall(x0, y);
                            break;
                        } else {
                            actualBomb.setFlameAreaUp(y + cellSize);
                            break;
                        }
                    }

                    if (y == (y0 - range*cellSize))
                        actualBomb.setFlameAreaUp(y);
                }

                for (int y = (y0 + cellSize); y <= (y0 + range*cellSize); y += cellSize ) {

                    objectCode = cellIsEmpty(x0, y);

                    if (objectCode != 0 ) {

                        if (objectCode == 1 && findPlayerByPosition(x0, y).equals(localPlayer)) {
                            actualBomb.setFlameAreaDown(y);
                            findPlayerById(localPlayersName).decreaseHP();
                            if (findPlayerById(localPlayersName).getHP() == 0)
                            {
                                GameGUI.gameOver();
                                Multiplayer_Packet quitPacket = new Multiplayer_Packet("QUIT");
                                Multiplayer.Send(quitPacket);
                            }
                            break;
                        } else if (objectCode == 2 && wallInCellIsExplodable(x0, y)) {
                            actualBomb.setFlameAreaDown(y);
                            deleteWall(x0, y);
                            break;
                        } else {
                            actualBomb.setFlameAreaDown(y - cellSize);
                            break;
                        }
                    }

                    if (y == (y0 + range*cellSize))
                        actualBomb.setFlameAreaDown(y);
                }
            }

            if ( actualBomb.getTimeOfExplosion() != -1
                    && (System.currentTimeMillis() - actualBomb.getTimeOfExplosion()) > actualBomb.getDurationOfFlame()) {
                if (actualBomb.getOwner().equals(localPlayersName))
                findPlayerById(actualBomb.getOwner()).decreaseActualBomb();
                bombIterator.remove();

            }
        }


        if ((Walls_Sent == false) && (Multiplayer.getIsServer()) && getGameStart() && initDone)
        {
            Walls_Sent = true;

            Multiplayer.Stop_Broadcast();

            Random Random_Selector = new Random();
            for (int i = 0; i < walls.size(); i++)
            {
                if (walls.get(i).explodable)
                {
                    int Number = Random_Selector.nextInt(100);
                    if (Number <= Reward_Probability)
                    {
                        Reward reward = new Reward("RANDOM", 0, 0);
                        walls.get(i).setReward(reward.getType());
                    }

                    Multiplayer_Packet WallPacket = new Multiplayer_Packet("", "WALL", walls.get(i).getxPos(), walls.get(i).getyPos());
                    WallPacket.setMessage(walls.get(i).getReward());
                    Multiplayer.Send(WallPacket);
                }
            }

            System.out.println("Wall Packet has been sent!");

        }

    }

    public static void remoteNewPlayer(String id, int x, int y) {
        Player newPlayer = new Player(id);
        newPlayer.setxPos(x);
        newPlayer.setyPos(y);
        players.add(newPlayer);
    }

    public static void remoteMovement(String id, int xPos, int yPos, int Image) {
        Player actualPlayer = findPlayerById(id);
        if (actualPlayer != null) {
            actualPlayer.setxPos_Image(xPos, Image);
            actualPlayer.setyPos_Image(yPos, Image);
        }
    }

    public static void remoteBombPlacement(String id, int xPos, int yPos) {
        bombs.add(new Bomb(xPos, yPos, findPlayerById(id).getBombRange(), id, findPlayerById(id).getBombTime()));
    }

    public static void remoteQuit(String id) {
        players.remove(findPlayerById(id));
    }

    public static void remoteWallPlacement(int xPos, int yPos, String reward) {
        walls.add(new Wall(true, xPos, yPos, reward));

    }

    public static Player findPlayerById(String id) {
        for (int idx = 0; idx < players.size(); idx++) {
            if (players.get(idx).id.equals(id)) {
                return players.get(idx);
            }
        }

        return null;
    }

    private static Player findPlayerByPosition(int xPos, int yPos) {
        for (Player actualPlayer : players) {
            boolean xIsTheSame = (abs(xPos - actualPlayer.getxPos(false)) < cellSize/2);
            boolean yIsTheSame = (abs(yPos - actualPlayer.getyPos(false)) < cellSize/2);
            if (xIsTheSame && yIsTheSame) {
                return actualPlayer;
            }
        }

        return null;
    }

    private static int findRewardByPosition(int xPos, int yPos)
    {
        int i = 0;
        for (Reward actualReward : rewards)
        {
            if (actualReward.getX() == xPos && actualReward.getY() == yPos)
            {
                return i;
            }

            i++;
        }

        return -1;
    }
}
