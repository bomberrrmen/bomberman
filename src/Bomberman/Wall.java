package Bomberman;

import javax.swing.*;
import java.awt.*;

public class Wall extends JComponent {

    private int xPos, yPos;
    private final int height = 50, width = 50;
    private String Reward = "";

    public boolean explodable;

    public Wall(boolean explodable) {
        this.explodable = explodable;
    }

    public Wall(boolean explodable, int xPos, int yPos)
    {
        this.explodable = explodable;
        this.xPos = xPos - width/2;
        this.yPos = yPos - height/2;
    }

    public Wall(boolean explodable, int xPos, int yPos, String Reward)
    {
        this.explodable = explodable;
        this.xPos = xPos - width/2;
        this.yPos = yPos - height/2;
        this.Reward = Reward;
    }

    public void paint(Graphics g, boolean gameOver) {

        Color myColor;

        if (explodable) {
            myColor = Color.ORANGE;

            if (gameOver) {
                int opacity = 50;
                myColor = new Color(255, 165, 0, opacity);
            }

        } else {
            myColor = Color.GRAY;

            if (gameOver) {
                int opacity = 50;
                myColor = new Color(128, 128, 128, opacity);
            }
        }

        g.setColor(myColor);
        g.drawRect(xPos, yPos, width ,height);
        g.fillRect(xPos, yPos, width ,height);

        /* Drawing the bricks */

        myColor = Color.BLACK;
        if (gameOver) {
            int opacity = 50;
            myColor = new Color(0, 0, 0, opacity);
        }

        g.setColor(myColor);
        g.drawLine((xPos), (yPos+height/3), (xPos+width), (yPos+height/3));
        g.drawLine((xPos), (yPos+2*height/3), (xPos+width), (yPos+2*height/3));              // Something like this
        g.drawLine((xPos+width/2), (yPos), (xPos+width/2), (yPos+height/3));                 //  _________
        g.drawLine((xPos+width/4), (yPos+height/3), (xPos+width/4), (yPos+2*height/3));      // |____|____|
        g.drawLine((xPos+3*width/4), (yPos+height/3), (xPos+3*width/4), (yPos+2*height/3));  // |__|___|__|
        g.drawLine((xPos+width/2), (yPos+2*height/3), (xPos+width/2), (yPos+height));        // |____|____|
    }

    public int getxPos() { return (xPos + width/2); }

    public void setxPos(int newxPos) { xPos = (newxPos - width/2); }

    public int getyPos() { return (yPos + height/2); }

    public void setyPos(int newyPos) { yPos = (newyPos - height/2); }

    public String getReward() { return Reward; }

    public void setReward(String Reward) { this.Reward = Reward; }

}
