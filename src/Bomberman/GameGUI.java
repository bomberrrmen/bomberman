package Bomberman;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.WindowConstants;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class GameGUI extends JPanel implements ActionListener, KeyListener {

    /* Objects needed to run the game  */
    private Timer timer = new Timer(5, this);

    /* Objects on the field / map */
    private static List<Player> players;
    private static List<Wall> walls;
    private static List<Bomb> bombs;
    private static List<Reward> rewards;
    private BufferedImage heartImage;

    /* Constants and variables related to the map */
    private final static int cellSize = 60;
    private final static int margin = cellSize/2;

    private static int cellNumberX = 10;
    private static int cellNumberY = 10;
    private static int windowSizeX = cellNumberX *cellSize + 2*margin;
    private static int windowSizeY = cellNumberY *cellSize + 2*margin;

    /* Menu related flags and containers */
    private static int actualOptionCursor = 1;
    private static int[] verticalCursors = {1, 1, 1, 1};
    private static boolean serverConfigDone = false;
    private static volatile boolean ConfigDone = false;      // For synchronization
    private static boolean allPlayersAreReady = false;
    private static volatile boolean gameStart = false;       // For synchronization
    private static boolean networkRoleIsServer = false;
    private static int numberOfPlayers = 0;
    private static String serverNameField = "";
    private static String playerNameField = "";
    private static boolean gameOver = false;

    GameGUI() {

        /* Initialization needed to run the game */
        timer.start();
        addKeyListener(this);
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);
        try {
            heartImage = ImageIO.read(new File(System.getProperty("user.dir") +
                                        System.getProperty("file.separator") + "icons" +
                                        System.getProperty("file.separator") + "heart.png"));
        } catch (Exception e) {
            System.out.println("Read of health point (heart) image failed!");
        }
    }

    public static void runGUI() {

        GameGUI gui = new GameGUI();

        JFrame jFrame = new JFrame();

        jFrame.setTitle("Bomberman");
        jFrame.setSize(windowSizeX, windowSizeY + margin);  // Plus margin needed
        jFrame.setVisible(true);
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrame.add(gui);

        /* Synchronization with the network config menu */
        while (true) {
            if (ConfigDone)
                break;
        }

        Thread multiplayer = new Thread(new Runnable() {
            public void run()
            {
                Multiplayer.Run(serverNameField, playerNameField, networkRoleIsServer, numberOfPlayers);
            }
        });
        multiplayer.start();

        GameEngine.initPlayersList();  // "Partially starting" the engine
        players = GameEngine.gimmeMyPlayers();

        /* Synchronization with the game start event */
        while (true) {
            if (gameStart)
                break;
        }


        for (Player actualPlayer:players) {

            if (actualPlayer.playerColor.equals(Color.BLUE)) {
                try {
                    if (actualPlayer != null) {
                        actualPlayer.AmountOfImages = 7;
                        for (int a = 0; a < 4; a++) {
                            for (int b = 0; b < 7; b++) {
                                actualPlayer.Images.add(ImageIO.read(new File(System.getProperty("user.dir") +
                                        System.getProperty("file.separator") + "icons" +
                                        System.getProperty("file.separator") + "kekszinu-" + b + "-" + a + ".png")));
                            }
                        }

                        actualPlayer.ActualImage = actualPlayer.Images.get(14);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (actualPlayer.playerColor.equals(Color.MAGENTA)) {
                try {
                    if (actualPlayer != null) {
                        actualPlayer.AmountOfImages = 9;
                        for (int a = 0; a < 4; a++) {
                            for (int b = 0; b < 9; b++) {
                                actualPlayer.Images.add(ImageIO.read(new File(System.getProperty("user.dir") +
                                        System.getProperty("file.separator") + "icons" +
                                        System.getProperty("file.separator") + "pinkszinu-0" + b + "-0" + a + ".png")));
                            }
                        }

                        actualPlayer.ActualImage = actualPlayer.Images.get(17);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (actualPlayer.playerColor.equals(Color.RED)) {
                try {
                    if (actualPlayer != null) {
                        actualPlayer.AmountOfImages = 9;
                        for (int a = 0; a < 4; a++) {
                            for (int b = 0; b < 9; b++) {
                                actualPlayer.Images.add(ImageIO.read(new File(System.getProperty("user.dir") +
                                        System.getProperty("file.separator") + "icons" +
                                        System.getProperty("file.separator") + "pirosszinu-0" + b + "-0" + a + ".png")));
                            }
                        }

                        actualPlayer.ActualImage = actualPlayer.Images.get(17);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (actualPlayer.playerColor.equals(Color.GREEN)) {
                try {
                    if (actualPlayer != null) {
                        actualPlayer.AmountOfImages = 7;
                        int mozgasasd = 2;
                        for (int b = 0; b < 7; b++) {
                            actualPlayer.Images.add(ImageIO.read(new File(System.getProperty("user.dir") +
                                    System.getProperty("file.separator") + "icons" +
                                    System.getProperty("file.separator") + "goblin-0" + b + "-0" + mozgasasd + ".png")));

                        }
                        mozgasasd = 3;
                        for (int b = 0; b < 7; b++) {
                            actualPlayer.Images.add(ImageIO.read(new File(System.getProperty("user.dir") +
                                    System.getProperty("file.separator") + "icons" +
                                    System.getProperty("file.separator") + "goblin-0" + b + "-0" + mozgasasd + ".png")));

                        }
                        mozgasasd = 0;
                        for (int b = 0; b < 7; b++) {
                            actualPlayer.Images.add(ImageIO.read(new File(System.getProperty("user.dir") +
                                    System.getProperty("file.separator") + "icons" +
                                    System.getProperty("file.separator") + "goblin-0" + b + "-0" + mozgasasd + ".png")));

                        }
                        mozgasasd = 1;
                        for (int b = 0; b < 7; b++) {
                            actualPlayer.Images.add(ImageIO.read(new File(System.getProperty("user.dir") +
                                    System.getProperty("file.separator") + "icons" +
                                    System.getProperty("file.separator") + "goblin-0" + b + "-0" + mozgasasd + ".png")));

                        }

                        actualPlayer.ActualImage = actualPlayer.Images.get(14);


                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


        windowSizeX = cellSize * cellNumberX + 2*margin;
        windowSizeY = cellSize * cellNumberY + 2*margin;
        jFrame.setSize(windowSizeX + 3*cellSize + margin, windowSizeY + margin);  // Plus margin needed

        Thread gameEngine = new Thread( new Runnable() {
            public void run() { GameEngine.runEngine(cellSize, cellNumberX, cellNumberY, margin); }
        });

        gameEngine.start();

        /* Synchronization with the game engine */
        while (true) {
            if (GameEngine.initDone)
                break;
        }

        walls = GameEngine.gimmeMyWalls();
        bombs = GameEngine.gimmeMyBombs();
        rewards = GameEngine.gimmeMyRewards();
    }

    public void paintComponent(Graphics g) {

        super.paintComponent(g);

        if (!serverConfigDone) {

            GameGUI.displayNetworkMenu(g);
            if (networkRoleIsServer) {
                GameGUI.displayServerSettings(g);
            }

        } else if (!ConfigDone) {

            GameGUI.displayNetworkMenu(g);
            if (networkRoleIsServer) {
                GameGUI.displayServerSettings(g);
            }
            GameGUI.displayPlayerSettings(g);
        } else if (!gameStart) {
            GameGUI.displayLobby(g);
        } else {

            paintGrid(g);


            if (walls != null) {
                for (Wall actualWall : walls) {
                    actualWall.paint(g, gameOver);
                }
            }



            if (bombs != null) {
                for (Bomb actualBomb : bombs) {
                    actualBomb.paint(g, gameOver);
                }
            }

            if (rewards != null) {
                for (Reward actualReward : rewards) {
                    actualReward.Paint(g, gameOver);
                }
            }

            if (players != null) {
                for (Player actualPlayer : players) {
                    actualPlayer.paint(g, gameOver);
                }
            }

            Color textColor = Color.BLACK;
            if (gameOver) {
                int opacity = 50;
                textColor = new Color(128, 128, 128, opacity);
            }
            g.setColor(textColor);
            g.setFont(new Font("Monospaced", Font.BOLD, 20));
            g.drawString("Health points", 2*margin + cellNumberX*cellSize, 50);

            Player localPlayer = GameEngine.findPlayerById(GameEngine.localPlayersName);
            int HP = 1;
            Bomb.RANGE range = Bomb.RANGE.DEFAULT_RANGE;
            if (localPlayer != null) {
                HP = localPlayer.getHP();
                range = localPlayer.getBombRange();
            }

            if (HP < 5) {
                for (int idx = 0; idx < HP; idx++) {
                    g.drawImage(heartImage, 2 * margin + cellNumberX * cellSize + 10 + 35 * idx, 60, null);
                }
            } else {
                g.drawString(HP + "x", 2 * margin + cellNumberX * cellSize + 10, 85);
                g.drawImage(heartImage, 2 * margin + cellNumberX * cellSize + 40, 60, null);
            }

            g.drawString("Bomb range: ", 2*margin + cellNumberX*cellSize, 130);
            String bombRangeText = "Default (1)";
            switch (range) {
                case DEFAULT_RANGE:
                    break;
                case ENHANCED_RANGE:
                    bombRangeText = "Enhanced! (2)";
                    break;
                case SUPER_RANGE:
                    bombRangeText = "SUPER (3)";
                    break;
                case HOLYSHIT_RANGE:
                    bombRangeText = "HOLY SH*T! (4)";
                    break;
            }
            g.drawString(bombRangeText, 2*margin + cellNumberX*cellSize + 10, 155);
        }

        if (gameOver) {
            g.setColor(Color.BLACK);
            g.setFont(new Font("Monospaced", Font.BOLD, 100));
            g.drawString("Game Over", windowSizeX/2 - 160, windowSizeY/2 + 30);
        }
    }

    private static void displayNetworkMenu(Graphics g) {
        g.setColor(Color.BLACK);

        g.setFont(new Font("Monospaced", Font.BOLD, 50));
        g.drawString("BOMBERMAN", 200, 50);

        g.setFont(new Font("Monospaced", Font.BOLD, 20));
        g.drawString("-- Choose network role -----------------", 20, 110);

        g.setFont(new Font("Monospaced", Font.BOLD, 15));
        g.drawString("Create a server", 70, 140);
        g.drawString("Connect to a server (Multiplayer only)", 70, 170);

        int cursor = 110 + 1 * 30;
        if (!networkRoleIsServer) {
            cursor = 110 + actualOptionCursor * 30;
        }
        g.drawString("> ", 50, cursor);
    }

    private static void displayServerSettings(Graphics g) {
        g.setColor(Color.BLACK);

        g.setFont(new Font("Monospaced", Font.BOLD, 20));
        g.drawString("-- Server settings ---------------------", 20, 200);

        g.setFont(new Font("Monospaced", Font.BOLD, 15));
        g.drawString("Server name: ", 70, 230);
        g.setColor(Color.GRAY);
        g.fillRect(180, 215, 200, 20);
        if (actualOptionCursor == 1) {
            g.setColor(Color.BLACK);
            g.drawRect(179, 214,201, 21);
        }

        g.setColor(Color.BLACK);
        g.drawString(serverNameField, 180, 230);

        g.setFont(new Font("Monospaced", Font.BOLD, 15));
        g.drawString("Number of players: ", 70, 260);

        g.setFont(new Font("Monospaced", Font.BOLD, 15));
        g.drawString("1  (Single player mode)", 100, 290);
        g.drawString("2", 100, 320);
        g.drawString("3", 100, 350);
        g.drawString("4", 100, 380);
        if (actualOptionCursor != 1) {
            g.drawString("> ", 70, 260 + (actualOptionCursor-1) * 30);
        }
    }

    private static void displayPlayerSettings(Graphics g) {

        int baseY = 200;

        if (networkRoleIsServer) {
            baseY = 380;
        }

        g.setColor(Color.BLACK);

        g.setFont(new Font("Monospaced", Font.BOLD, 20));
        g.drawString("-- Player settings ---------------------", 20, baseY);

        g.setFont(new Font("Monospaced", Font.BOLD, 15));
        g.drawString("Your name: ", 70, baseY + 30);

        g.setColor(Color.GRAY);
        g.fillRect(180, baseY + 15, 200, 20);
        if (actualOptionCursor == 1) {
            g.setColor(Color.BLACK);
            g.drawRect(179, baseY + 14,201, 21);
        }

        g.setColor(Color.BLACK);
        g.drawString(playerNameField, 180, baseY + 30);
    }

    private static void displayLobby(Graphics g) {
        g.setColor(Color.BLACK);

        g.setFont(new Font("Monospaced", Font.BOLD, 50));
        g.drawString("BOMBERMAN", 200, 50);

        int hCursor = 140;
        switch (actualOptionCursor) {
            case 1: hCursor = 140; break;
            case 2: hCursor = 170; break;
            case 3: hCursor = 200; break;
            case 4:
                if (networkRoleIsServer) {
                    hCursor = 230;
                } else {
                    hCursor = 290;
                }
                break;
            case 5: hCursor = 290;
        }
        g.setFont(new Font("Monospaced", Font.BOLD, 15));
        g.drawString("> ", 50, hCursor);

        g.setFont(new Font("Monospaced", Font.BOLD, 20));
        g.drawString("-- Settings for your player ------------", 20, 110);

        g.setFont(new Font("Monospaced", Font.BOLD, 15));
        g.drawString("Avatar / color: ", 70, 140);

        int vCursor1 = 230;
        int vCursorLength = 35;
        Player localPlayer = GameEngine.findPlayerById(playerNameField);
        switch (verticalCursors[0]) {
            case 1:
                vCursor1 = 230;
                vCursorLength = 35;
                if (localPlayer != null)
                    localPlayer.playerColor = Color.BLUE;
                break;
            case 2:
                vCursor1 = 320;
                vCursorLength = 25;
                if (localPlayer != null)
                    localPlayer.playerColor = Color.RED;
                break;
            case 3:
                vCursor1 = 400;
                vCursorLength = 45;
                if (localPlayer != null)
                    localPlayer.playerColor = Color.GREEN;
                break;
            case 4:
                vCursor1 = 500;
                vCursorLength = 35;
                if (localPlayer != null)
                    localPlayer.playerColor = Color.MAGENTA;
                break;
        }
        g.drawLine(vCursor1, 148, vCursor1 + vCursorLength, 148);

        g.drawString("BLUE", 230, 140);
        g.setColor(Color.BLUE);
        g.fillRect(270, 125, 20, 20);

        g.setColor(Color.BLACK);
        g.drawString("RED", 320, 140);
        g.setColor(Color.RED);
        g.fillRect(350, 125, 20, 20);

        g.setColor(Color.BLACK);
        g.drawString("GREEN", 400, 140);
        g.setColor(Color.GREEN);
        g.fillRect(450, 125, 20, 20);

        g.setColor(Color.BLACK);
        g.drawString("PINK", 500, 140);
        g.setColor(Color.MAGENTA);
        g.fillRect(540, 125, 20, 20);

        g.setColor(Color.BLACK);
        g.drawString("Position on the map: ", 70, 170);
        int vCursor2 = 290;
        switch (verticalCursors[1]) {
            case 1:
                vCursor2 = 290;
                if (localPlayer != null)
                    localPlayer.initPosOnMap = "A";
                break;
            case 2:
                vCursor2 = 320;
                if (localPlayer != null)
                    localPlayer.initPosOnMap = "B";
                break;
            case 3:
                vCursor2 = 350;
                if (localPlayer != null)
                    localPlayer.initPosOnMap = "C";
                break;
            case 4:
                vCursor2 = 380;
                if (localPlayer != null)
                    localPlayer.initPosOnMap = "D";
                break;
        }
        g.drawLine(vCursor2, 178, vCursor2 + 10, 178);
        g.drawString("A", 290, 170);
        g.drawString("B", 320, 170);
        g.drawString("C", 350, 170);
        g.drawString("D", 380, 170);

        g.setColor(Color.BLACK);
        g.drawString("Ready: ", 70, 200);
        String readyText = "NOT READY  (Press Enter to toggle)";
        if (verticalCursors[2] == 2) {
            readyText = "READY      (Press Enter to toggle)";
        }
        g.drawString(readyText, 130, 200);

        if (networkRoleIsServer) {
            if (!GameGUI.allPlayersAreReady()) {
                g.setColor(Color.GRAY);
            }
            g.drawString("START", 70, 230);
        }



        int mapSettingsBaseY = 260;
        g.setColor(Color.BLACK);
        g.setFont(new Font("Monospaced", Font.BOLD, 20));
        g.drawString("-- Map settings ------------------------", 20, mapSettingsBaseY);
        g.setFont(new Font("Monospaced", Font.BOLD, 15));

        g.drawString("Map size: ", 70, mapSettingsBaseY + 30);
        int vCursor3 = 230;
        int vCursorLength2 = 26;

        switch (verticalCursors[3]) {
            case 1:
                vCursor3 = 230;  // 5x7 option
                cellNumberX = 9;
                cellNumberY = 7;
                break;
            case 2:
                vCursor3 = 280;  // 7x7 option
                cellNumberX = 9;
                cellNumberY = 9;
                break;
            case 3:
                vCursor3 = 330;  // 7x13 option
                cellNumberX = 15;
                cellNumberY = 9;
                vCursorLength2 = 36;
        }
        g.drawLine(vCursor3, mapSettingsBaseY + 38, vCursor3 + vCursorLength2, mapSettingsBaseY + 38);

        g.drawString("5x7", 230, mapSettingsBaseY + 30);

        g.setColor(Color.BLACK);
        g.drawString("7x7", 280, mapSettingsBaseY + 30);

        g.setColor(Color.BLACK);
        g.drawString("7x13", 330, mapSettingsBaseY + 30);

        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(450, mapSettingsBaseY + 30, 8*cellNumberX, 8*cellNumberY);

        g.setColor(Color.BLACK);
        g.setFont(new Font("Monospaced", Font.BOLD, 12));
        g.drawString(String.valueOf(cellNumberX-2), 450 + 3*cellNumberX, mapSettingsBaseY + 20);
        g.drawLine(450, mapSettingsBaseY + 25, 450 + 8*cellNumberX - 1, mapSettingsBaseY + 25);
        g.drawString(String.valueOf(cellNumberY-2), 430, mapSettingsBaseY + 20 + 6*cellNumberY);
        g.drawLine(445, mapSettingsBaseY + 30, 445, mapSettingsBaseY + 30 + 8*cellNumberY - 1);
        g.drawString("A", 452, mapSettingsBaseY + 42);
        g.drawString("B", 452, mapSettingsBaseY + 28 + 8*cellNumberY);
        g.drawString("D", 450 + 8*cellNumberX - 12, mapSettingsBaseY + 42);
        g.drawString("C", 450 + 8*cellNumberX - 12, mapSettingsBaseY + 28 + 8*cellNumberY);

        int connectedPlayersBaseY = 400;
        g.setColor(Color.BLACK);
        g.setFont(new Font("Monospaced", Font.BOLD, 20));
        g.drawString("-- Connected players -------------------", 20, connectedPlayersBaseY);
        g.setFont(new Font("Monospaced", Font.BOLD, 15));
        if (players != null) {
            int index = 1;
            for (Player actualPlayer : players) {
                g.drawString("Player #" + index + ": ", 70, connectedPlayersBaseY + 10 + index*20);
                g.drawString(actualPlayer.id, 170, connectedPlayersBaseY + 10 + index*20);
                if (actualPlayer.id.equals(playerNameField))
                    g.drawString("(You)", 370, connectedPlayersBaseY + 10 + index * 20);
                if (actualPlayer.playerColor != null) {
                    g.setColor(actualPlayer.playerColor);
                    g.fillRect(425, connectedPlayersBaseY + 10 + index * 20 - 13, 16, 16);
                }
                if (actualPlayer.initPosOnMap != null) {
                    g.setColor(Color.BLACK);
                    g.drawString(actualPlayer.initPosOnMap, 450, connectedPlayersBaseY + 10 + index * 20);
                }
                if (actualPlayer.ready)
                    readyText = "READY";
                else
                    readyText = "NOT READY";
                g.drawString(readyText, 470, connectedPlayersBaseY + 10 + index * 20);

                index++;
            }

            if (players.size() != numberOfPlayers) {
                for (index = players.size() + 1; index <= numberOfPlayers; index++) {
                    g.drawString("Player #" + index + ": ", 70, connectedPlayersBaseY + 10 + index*20);
                    g.drawString("...", 170, connectedPlayersBaseY + 10 + index*20);
                }
            }
        }
    }

    public static boolean allPlayersAreReady() {

        boolean ready = true;

        if (players != null) {
            for (Player actualPlayer : players)
                if (!actualPlayer.ready)
                    ready = false;

            if (ready) {
                GameGUI.allPlayersAreReady = true;
                return true;
            } else {
                GameGUI.allPlayersAreReady = false;
                return false;
            }
        } else {
            return false;
        }
    }

    private void paintGrid(Graphics g) {

        if (gameOver)
            return;

        g.setColor(Color.GRAY);
        for (int vertical_idx = 0; vertical_idx < cellNumberX+1; vertical_idx++) {
            g.drawLine((margin + vertical_idx*cellSize), (margin),
                       (margin + vertical_idx*cellSize), (windowSizeY - margin));
        }
        for (int horizontal_idx = 0; horizontal_idx < cellNumberY+1; horizontal_idx++) {
            g.drawLine((margin), (margin + horizontal_idx*cellSize),
                       (windowSizeX - margin), (margin + horizontal_idx*cellSize));
        }
    }

    /* Action handling */
    @Override
    public void actionPerformed(ActionEvent actionEvent) {

        if (gameOver)
            return;

        GameEngine.updateGameState();
        repaint();
    }

    /* Keyboard handling */
    @Override
    public void keyTyped(KeyEvent keyEvent) {}

    @Override
    public void keyPressed(KeyEvent keyEvent) {

        if (gameOver)
            return;

        int keyCode = keyEvent.getKeyCode();

        if (!serverConfigDone) {
            switch (keyCode) {

                case KeyEvent.VK_UP:
                    if (actualOptionCursor != 1) actualOptionCursor -= 1;
                    break;

                case KeyEvent.VK_DOWN:
                    if (!networkRoleIsServer) {
                        if (actualOptionCursor != 2) actualOptionCursor += 1;
                    } else {
                        if (actualOptionCursor != 5) actualOptionCursor += 1;
                    }
                    break;

                case KeyEvent.VK_ENTER:
                    if (!networkRoleIsServer) {
                        if (actualOptionCursor == 1) {
                            networkRoleIsServer = true;
                        } else {
                            setServerConfigDone();
                        }
                        break;
                    } else {
                        if (actualOptionCursor != 1 && serverNameField.length() != 0) {
                            numberOfPlayers = actualOptionCursor-1;
                            setServerConfigDone();
                        }
                    }
                    break;
                case KeyEvent.VK_BACK_SPACE:
                    if (networkRoleIsServer && actualOptionCursor == 1) {
                        if (serverNameField.length() != 0) {
                            serverNameField = serverNameField.substring(0, serverNameField.length() - 1);
                        }
                    }
                    break;

                default:
                    if (((keyCode >= KeyEvent.VK_A) && (keyCode <= KeyEvent.VK_Z))  ||
                            (keyCode >= KeyEvent.VK_0) && (keyCode <= KeyEvent.VK_9)) {
                        if (networkRoleIsServer && (actualOptionCursor == 1)) {
                            if (serverNameField.length() < 21)
                                serverNameField = serverNameField.concat(KeyEvent.getKeyText(keyCode));
                        }
                    } else if ((keyCode >= KeyEvent.VK_NUMPAD0) && (keyCode <= KeyEvent.VK_NUMPAD9)) {
                        if (networkRoleIsServer && (actualOptionCursor == 1)) {
                            if (serverNameField.length() < 21) {
                                String keyString = KeyEvent.getKeyText(keyCode);
                                serverNameField = serverNameField.concat(keyString.substring(keyString.length() - 1));
                            }
                        }
                    }
                    break;
            }
        } else if (!ConfigDone) {
            switch (keyCode) {

                case KeyEvent.VK_UP:
                    if (actualOptionCursor != 1) actualOptionCursor -= 1;
                    break;

                case KeyEvent.VK_DOWN:
                    if (actualOptionCursor != 1) actualOptionCursor += 1;
                    break;

                case KeyEvent.VK_ENTER:
                    if (actualOptionCursor == 1 && playerNameField.length() != 0) {
                        setConfigDone();
                    }
                    break;
                case KeyEvent.VK_BACK_SPACE:
                    if (actualOptionCursor == 1) {
                        if (playerNameField.length() != 0) {
                            playerNameField = playerNameField.substring(0, playerNameField.length() - 1);
                        }
                    }
                    break;
                default:
                    if (((keyCode >= KeyEvent.VK_A) && (keyCode <= KeyEvent.VK_Z))  ||
                            (keyCode >= KeyEvent.VK_0) && (keyCode <= KeyEvent.VK_9)) {
                        if (actualOptionCursor == 1) {
                            if (playerNameField.length() < 21)
                                playerNameField = playerNameField.concat(KeyEvent.getKeyText(keyCode));
                        }
                    } else if ((keyCode >= KeyEvent.VK_NUMPAD0) && (keyCode <= KeyEvent.VK_NUMPAD9)) {
                        if (actualOptionCursor == 1) {
                            if (playerNameField.length() < 21) {
                                String keyString = KeyEvent.getKeyText(keyCode);
                                playerNameField = playerNameField.concat(keyString.substring(keyString.length() - 1));
                            }
                        }
                    }
                    break;
            }
        } else if (!gameStart) {
            switch (keyCode) {

                case KeyEvent.VK_UP:
                    if (actualOptionCursor != 1) actualOptionCursor -= 1;
                    break;

                case KeyEvent.VK_DOWN:
                    if (networkRoleIsServer) {
                        if (actualOptionCursor != 5) actualOptionCursor += 1;
                    } else {
                        if (actualOptionCursor != 4) actualOptionCursor += 1;
                    }
                    break;

                case KeyEvent.VK_LEFT:
                    int optionIndex = 0;
                    if (actualOptionCursor == 2) {
                        optionIndex = 1;
                    } else if (networkRoleIsServer && actualOptionCursor == 5 ||
                                !networkRoleIsServer && actualOptionCursor == 4) {
                        optionIndex = 3;
                    }
                    if (verticalCursors[optionIndex] != 1) {
                        verticalCursors[optionIndex] -= 1;

                        Multiplayer_Packet configChangePacket = new Multiplayer_Packet("CONFIG");
                        configChangePacket.setMessage(generateConfigMessage());
                        Multiplayer.Send(configChangePacket);
                    }
                    break;

                case KeyEvent.VK_RIGHT:
                    optionIndex = 0;
                    if (actualOptionCursor == 2) {
                        optionIndex = 1;
                    } else if (networkRoleIsServer && actualOptionCursor == 5 ||
                            !networkRoleIsServer && actualOptionCursor == 4) {
                        optionIndex = 3;
                    }
                    int maxOptions = 4;
                    if (optionIndex == 3)
                        maxOptions = 3;
                    if (verticalCursors[optionIndex] != maxOptions) {
                        verticalCursors[optionIndex] += 1;

                        Multiplayer_Packet configChangePacket = new Multiplayer_Packet("CONFIG");
                        configChangePacket.setMessage(generateConfigMessage());
                        Multiplayer.Send(configChangePacket);
                    }
                    break;

                case KeyEvent.VK_ENTER:
                    if (actualOptionCursor == 1) {
                        // Set color of player
                    } else if (actualOptionCursor == 2) {
                        // Set position on map
                    } else if (actualOptionCursor == 3) {
                        if (verticalCursors[2] == 2) {
                            verticalCursors[2] = 1;
                            Player localPlayer = GameEngine.findPlayerById(playerNameField);
                            if (localPlayer != null)
                                localPlayer.ready = false;
                        }
                        else {
                            verticalCursors[2] = 2;
                            Player localPlayer = GameEngine.findPlayerById(playerNameField);
                            if (localPlayer != null)
                                localPlayer.ready = true;
                        }

                        Multiplayer_Packet configChangePacket = new Multiplayer_Packet("CONFIG");
                        configChangePacket.setMessage(generateConfigMessage());
                        Multiplayer.Send(configChangePacket);
                    } else {
                        if (allPlayersAreReady) {
                            Multiplayer_Packet startGamePacket = new Multiplayer_Packet("START");
                            Multiplayer.Send(startGamePacket);
                            startGame();
                        }
                    }
                    break;
            }
        } else {

            switch (keyCode) {
                case KeyEvent.VK_LEFT:
                    GameEngine.processKeyPressed(GameEngine.KEY_COMMANDS.MOVE_LEFT);
                    break;

                case KeyEvent.VK_RIGHT:
                    GameEngine.processKeyPressed(GameEngine.KEY_COMMANDS.MOVE_RIGHT);
                    break;

                case KeyEvent.VK_UP:
                    GameEngine.processKeyPressed(GameEngine.KEY_COMMANDS.MOVE_UP);
                    break;

                case KeyEvent.VK_DOWN:
                    GameEngine.processKeyPressed(GameEngine.KEY_COMMANDS.MOVE_DOWN);
                    break;

                case KeyEvent.VK_SPACE:
                    GameEngine.processKeyPressed(GameEngine.KEY_COMMANDS.PLACE_BOMB);
                    break;
            }
        }
    }

    private String generateConfigMessage() {
        String configMessage = "";

        switch (verticalCursors[1]) {
            case 1:
                configMessage = configMessage.concat("A");
                break;
            case 2:
                configMessage = configMessage.concat("B");
                break;
            case 3:
                configMessage = configMessage.concat("C");
                break;
            case 4:
                configMessage = configMessage.concat("D");
                break;
        }
        
        configMessage = configMessage.concat("|");

        switch (verticalCursors[0]) {
            case 1:
                configMessage = configMessage.concat("Blue");
                break;
            case 2:
                configMessage = configMessage.concat("Red");
                break;
            case 3:
                configMessage = configMessage.concat("Green");
                break;
            case 4:
                configMessage = configMessage.concat("Pink");
                break;
        }

        configMessage = configMessage.concat("|");

        Player localPlayer = GameEngine.findPlayerById(GameEngine.localPlayersName);
        if (localPlayer == null)
            return "";

        configMessage = configMessage + localPlayer.ready + "|";

        switch (verticalCursors[3]) {
            case 1:
                configMessage = configMessage.concat("5x7");
                break;
            case 2:
                configMessage = configMessage.concat("7x7");
                break;
            case 3:
                configMessage = configMessage.concat("7x13");
                break;
        }

        configMessage = configMessage.concat("|");

        return configMessage;
    }

    private synchronized void setServerConfigDone() {
        serverConfigDone = true;
        actualOptionCursor = 1;
        notifyAll();
    }

    private synchronized void setConfigDone() {
        ConfigDone = true;
        notifyAll();
    }

    private synchronized void startGame() {
        gameStart = true;
        notifyAll();
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {

        if (gameOver)
            return;

        int keyCode = keyEvent.getKeyCode();

        if (!ConfigDone) {
            return;
        }

        switch (keyCode) {
            case KeyEvent.VK_LEFT:
                GameEngine.processKeyReleased(GameEngine.KEY_COMMANDS.MOVE_LEFT);
                break;

            case KeyEvent.VK_RIGHT:
                GameEngine.processKeyReleased(GameEngine.KEY_COMMANDS.MOVE_RIGHT);
                break;

            case KeyEvent.VK_UP:
                GameEngine.processKeyReleased(GameEngine.KEY_COMMANDS.MOVE_UP);
                break;

            case KeyEvent.VK_DOWN:
                GameEngine.processKeyReleased(GameEngine.KEY_COMMANDS.MOVE_DOWN);
                break;

            case KeyEvent.VK_SPACE:
                GameEngine.processKeyReleased(GameEngine.KEY_COMMANDS.PLACE_BOMB);
                break;
        }
    }

    public static void setMapSize(String stringCode) {

        switch (stringCode) {
            case "5x7":
                cellNumberX = 9;
                cellNumberY = 7;
                verticalCursors[3] = 1;
                break;
            case "7x7":
                cellNumberX = 9;
                cellNumberY = 9;
                verticalCursors[3] = 2;
                break;
            case "7x13":
                cellNumberX = 15;
                cellNumberY = 9;
                verticalCursors[3] = 3;
                break;
        }
    }

    public static void remoteStartGame() {
        gameStart = true;
    }

    public static void gameOver() {

        gameOver = true;
    }

    public static boolean getGameStart() { return gameStart; }

}
