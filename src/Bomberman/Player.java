package Bomberman;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.List;

public class Player extends JComponent {

    private int xPos, yPos;
    private int xStep = 0, yStep = 0;
    private int xStepRemainder = 0, yStepRemainder = 0;
    private final int playerHeight = 64, playerWidth = 64;
    public String id;
    public String initPosOnMap;
    public Color playerColor;
    public boolean ready = false;
    private Bomb.RANGE bombRange = Bomb.RANGE.DEFAULT_RANGE;
    private int maxBomb = 1;
    private int actualBomb = 0;
    private int HP = 1;
    private int Speed = 0;
    private int BombTime = 3000;

    public int ActualDirectionNumber;
    public int ActualImageNumber;
    public int AmountOfImages;
    public List<BufferedImage> Images = new LinkedList<>();
    public BufferedImage ActualImage;

    Player(String id) {
        this.id = id;
    }

    public void paint(Graphics g, boolean gameOver) {

        Color myColor = playerColor;
        if (gameOver) {
            int opacity = 50;
            myColor = new Color(0, 0, 255, opacity);
        }

        /*g.setColor(myColor);
        g.drawRect(xPos, yPos, playerWidth, playerHeight);
        g.fillRect(xPos, yPos, playerWidth, playerHeight);*/

        g.drawImage(ActualImage, xPos, yPos, null);
    }

    public int getxPos(boolean forBomb) {

        int xPosToReturn;

        if (forBomb) xPosToReturn = (xPos + playerWidth/2 - (xStep - xStepRemainder));  // Last cell's X position
        else xPosToReturn = (xPos + playerWidth/2 + xStepRemainder);                    // Next cell's X position

        return xPosToReturn;
    }

    public int getActualxPos() {return xPos + playerWidth/2;}

    public void setxPos(int newxPos) {

        xPos = (newxPos - playerWidth/2);

    }

    public void setxPos_Image(int newxPos, int Image)
    {
        xPos = (newxPos - playerWidth/2);

        ActualImage = Images.get(Image);

    }

    public int getyPos(boolean forBomb) {

        int yPosToReturn;

        if (forBomb) yPosToReturn = (yPos + playerHeight/2 - (yStep - yStepRemainder));  // Last cell's Y position
        else yPosToReturn = (yPos + playerHeight/2 + yStepRemainder);                    // Next cell's Y position

        return yPosToReturn;
    }

    public int getActualyPos() {return yPos + playerHeight/2;}

    public void setyPos(int newyPos) {
        yPos = (newyPos - playerHeight/2);

    }

    public void setyPos_Image(int newyPos, int Image) {
        yPos = (newyPos - playerHeight/2);

        ActualImage = Images.get(Image);

    }

    public void setxStep(int newxVel) {

        if (xStepRemainder == 0) {
            xStep          = newxVel;
            xStepRemainder = newxVel;

            if (newxVel > 0)
            {
                ActualDirectionNumber = 3;
                ActualImageNumber = 0;
            }
            else
            {
                ActualDirectionNumber = 1;
                ActualImageNumber = 0;
            }
        }
    }

    public void setyStep(int newyVel) {

        if (yStepRemainder == 0) {
            yStep          = newyVel;
            yStepRemainder = newyVel;

            if (newyVel > 0)
            {
                ActualDirectionNumber = 2;
                ActualImageNumber = 0;
            }
            else
            {
                ActualDirectionNumber = 0;
                ActualImageNumber = 0;
            }
        }
    }

    public void updatePos() {

        if (Math.abs(xStepRemainder) > Speed/2) {  // xVel helps to move smoothly between cells
            xPos           += (xStepRemainder > 0) ? (+1 + Speed) : (-1 - Speed);
            xStepRemainder += (xStepRemainder > 0) ? (-1 - Speed) : (+1 + Speed);

            Multiplayer_Packet movementPacket = new Multiplayer_Packet("MOVEMENT");
            movementPacket.setXY(xPos + playerWidth/2, yPos + playerHeight/2);
            movementPacket.setMessage(String.valueOf(AmountOfImages * ActualDirectionNumber + ActualImageNumber));
            Multiplayer.Send(movementPacket);

            if ((xStepRemainder%10) == 0)
            {

                ActualImage = Images.get(AmountOfImages * ActualDirectionNumber + ActualImageNumber);

                if (ActualImageNumber < AmountOfImages - 1)
                {
                    ActualImageNumber++;
                }
                else
                {
                    ActualImageNumber = 0;
                }
            }
        } else {
            xStep = 0;  // xStep holds the last step's value until its finished
            xStepRemainder = 0;
        }

        if (Math.abs(yStepRemainder) > Speed/2) {  // yVel helps to move smoothly between cells
            yPos           += (yStepRemainder > 0) ? (+1 + Speed) : (-1 - Speed);
            yStepRemainder += (yStepRemainder > 0) ? (-1 - Speed) : (+1 + Speed);

            Multiplayer_Packet movementPacket = new Multiplayer_Packet("MOVEMENT");
            movementPacket.setXY(xPos + playerWidth/2, yPos + playerHeight/2);
            movementPacket.setMessage(String.valueOf(AmountOfImages * ActualDirectionNumber + ActualImageNumber));
            Multiplayer.Send(movementPacket);

            if ((yStepRemainder%10) == 0)
            {

                ActualImage = Images.get(AmountOfImages * ActualDirectionNumber + ActualImageNumber);

                if (ActualImageNumber < AmountOfImages - 1)
                {
                    ActualImageNumber++;
                }
                else
                {
                    ActualImageNumber = 0;
                }
            }
        } else {
            yStep = 0;  // yStep holds the last step's value until its finished
            yStepRemainder = 0;
        }
    }

    public Bomb.RANGE getBombRange() {
        return bombRange;
    }

    public void setBombRange(Bomb.RANGE newRange) {
        bombRange = newRange;
    }

    public int getMaxBomb() {return maxBomb;}

    public int getActualBomb() {return actualBomb;}

    public void increaseActualBomb() {actualBomb++; }

    public void decreaseActualBomb() {actualBomb--;}

    public void increaseMaxBomb() {maxBomb++; }

    public String getID() {return id;}

    public int getHP() {return HP; }

    public void increaseHP() {
        if (HP < 9)
        {
            HP++;
        }
    }

    public void decreaseHP() { HP--; }

    public void increaseSpeed()
    {
        if (Speed < 4)
        {
            Speed++;
        }
    }

    public void decreaseBombTime()
    {
        if (BombTime > 1000)
        {
            BombTime -= 1000;
        }
    }

    public int getBombTime() { return BombTime; }
}
