package Bomberman;

public class Multiplayer_Packet {


    private String ID = "";
    private String Type;
    private int X = 0;
    private int Y = 0;
    private String Message;


    // Constructors:
    Multiplayer_Packet()
    {
        Type = "NOTHING";
    }

    Multiplayer_Packet(String Type)
    {
        this.Type = Type_Check(Type);
    }

    Multiplayer_Packet(String ID, String Type, int X, int Y)
    {
        this.ID = ID;
        this.Type = Type_Check(Type);
        this.X = X;
        this.Y = Y;
    }

    Multiplayer_Packet(String ID, String Type, int X, int Y, String Message)
    {
        this.ID = ID;
        this.Type = Type_Check(Type);
        this.X = X;
        this.Y = Y;
        this.Message = Message;
    }

    // Method, that serialize the private variables into a single string:
    String Serialize()
    {
        String Data = ID + "," + Type + "," + X + "," + Y + "," + Message + ",";
        return Data;
    }

    // Method, that deserialize the given string into the private variables:
    void Deserialize(String Data)
    {
        String[] Values = Data.split(",");
        ID = Values[0];
        Type = Values[1];
        X = Integer.valueOf(Values[2]);
        Y = Integer.valueOf(Values[3]);
        Message = Values[4];

    }


    // Method, that checks if the given packet type is valid:
    String Type_Check(String Type)
    {
        switch (Type)
        {
            case "CONNECT":
            case "NEW_PLAYER":
            case "MOVEMENT":
            case "BOMB":
            case "QUIT":
            case "CONFIG":
            case "WALL":
            case "START": return Type;
            default: return "NOTHING";
        }
    }

    // Getter functions:
    public String getID()
    {
        return ID;
    }


   public String getType()
    {
        return Type;
    }

    public int getX()
    {
        return X;
    }

    public int getY()
    {
        return Y;
    }

    public String getMessage() { return Message; }

    // Setter functions:
    public void setID(String ID)
    {
        this.ID = ID;
    }


   public void setXY(int X, int Y)
    {
        this.X = X;
        this.Y = Y;
    }

    public void setMessage(String Message)
    {
        this.Message = Message;
    }


}
