package Bomberman;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;

public class Bomb extends JComponent {

    private int xPos, yPos;
    private final int height = 40, width = 40;
    private final int flameHeight = 60, flameWidth = 60;
    private int timeToBumm = 3000;            // Milliseconds
    private final int durationOfFlame = 200;        // Milliseconds
    private long timeOfPlacement;
    private long timeOfExplosion = -1;
    private String Owner;
    private BufferedImage Image;
    private double Time = 0;
    private DecimalFormat df2 = new DecimalFormat("#.##");

    enum RANGE { DEFAULT_RANGE, ENHANCED_RANGE, SUPER_RANGE, HOLYSHIT_RANGE}

    private RANGE range;
    private int flameAreaLeft = 0;
    private int flameAreaRight = 0;
    private int flameAreaUp = 0;
    private int flameAreaDown = 0;

    Bomb(int newxPos, int newyPos, RANGE newRange, String Owner, int timeToBumm) {
        xPos = (newxPos - width/2);
        yPos = (newyPos - height/2);
        range = newRange;
        timeOfPlacement = System.currentTimeMillis();
        this.Owner = Owner;
        this.timeToBumm = timeToBumm;

        try
        {
            Image = ImageIO.read(new File(System.getProperty("user.dir") +
                    System.getProperty("file.separator") + "icons" +
                    System.getProperty("file.separator") + "Bomb2.png"));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void paint(Graphics g, boolean gameOver) {



        Color myColor;

        /* If bomb has not exploded yet */
        if (timeOfExplosion == -1) {

            g.drawImage(Image, xPos, yPos, this);

            g.setFont(new Font("Monospaced", Font.BOLD, 10));
            String Text = df2.format(Time);
            g.drawString(Text, xPos, yPos);
        }

        /* If bomb has exploded but the flame has not gone out yet */
        else if ((System.currentTimeMillis() - timeOfExplosion) < durationOfFlame) {
            // Draw flame
            myColor = Color.RED;
            if (gameOver) {
                int opacity = 50;
                myColor = new Color(255, 0, 0, opacity);
            }
            g.setColor(myColor);
            int redFlameXPos1 = flameAreaLeft - flameWidth/2;
            int redFlameYPos1 = yPos + height/2 - flameHeight/2;
            int redFlameXPos2 = xPos + width/2 - flameWidth/2;
            int redFlameYPos2 = flameAreaUp - flameHeight/2;
            g.fillRect( redFlameXPos1,
                        redFlameYPos1,
                    (flameAreaRight - flameAreaLeft) + flameWidth,                  // Vertical
                        flameHeight);
            g.fillRect( redFlameXPos2,
                        redFlameYPos2,
                        flameWidth,
                    (flameAreaDown - flameAreaUp) + flameHeight);                   // Horizontal

            myColor = Color.ORANGE;
            if (gameOver) {
                int opacity = 50;
                myColor = new Color(255, 165, 0, opacity);
            }
            g.setColor(myColor);
            int orangeFlameXPos1 = xPos + width/2 - flameWidth/4;
            int orangeFlameXPos2 = flameAreaLeft - flameWidth/4;
            int orangeFlameYPos1 = flameAreaUp - flameHeight/4;
            int orangeFlameYPos2 = yPos + height/2 - flameHeight/4;
            g.fillRect( orangeFlameXPos1,
                        orangeFlameYPos1,
                    flameWidth/2,
                    (flameAreaDown - flameAreaUp) + 2*flameHeight/4);               // Vertical
            g.fillRect( orangeFlameXPos2,
                        orangeFlameYPos2,
                    (flameAreaRight - flameAreaLeft) + 2*flameWidth/4,
                    flameHeight/2);                                                 // Horizontal

            myColor = Color.YELLOW;
            if (gameOver) {
                int opacity = 50;
                myColor = new Color(255, 255, 0, opacity);
            }
            g.setColor(myColor);
            int yellowFlameXPos1 = xPos + width/2 - flameWidth/8;
            int yellowFlameXPos2 = flameAreaLeft - flameWidth/8;
            int yellowFlameYPos1 = flameAreaUp - flameHeight/8;
            int yellowFlameYPos2 = yPos + height/2 - flameHeight/8;
            g.fillRect( yellowFlameXPos1,
                        yellowFlameYPos1,
                    flameWidth/4,
                    (flameAreaDown - flameAreaUp) + 2*flameHeight/8);               // Vertical
            g.fillRect( yellowFlameXPos2,
                        yellowFlameYPos2,
                    (flameAreaRight - flameAreaLeft) + 2*flameWidth/8,
                    flameHeight/4);                                                 // Horizontal
        }
        /* The bomb did explode and disappeared */
        // else {
            // Draw nothing, wait for GameEngine to delete this object
        // }
    }

    public boolean update() {

        boolean justExploded = false;

        /* If the bomb has not exploded yet but should now */
        if ((timeOfExplosion == -1) && ((System.currentTimeMillis() - timeOfPlacement) > timeToBumm)) {
                timeOfExplosion = System.currentTimeMillis();
                justExploded = true;
        }
        else
        {
            long Mil = timeToBumm - (System.currentTimeMillis() - timeOfPlacement);
            Time = Mil/1000.0;
        }

        return justExploded;
    }

    public int getxPos() {
        return xPos + width/2;
    }

    public int getyPos() {
        return yPos + height/2;
    }

    public long getTimeOfExplosion() {
        return timeOfExplosion;
    }

    public int getDurationOfFlame() {
        return durationOfFlame;
    }

    public int getRangeValue() {

        int rangeValue = 0;

        switch (range) {
            case DEFAULT_RANGE:
                rangeValue = 1;
                break;

            case ENHANCED_RANGE:
                rangeValue = 2;
                break;

            case SUPER_RANGE:
                rangeValue = 3;
                break;

            case HOLYSHIT_RANGE:
                rangeValue = 4;
                break;
        }

        return rangeValue;
    }

    public void setFlameAreaLeft(int newFlameAreaLeft) {
        if (newFlameAreaLeft > 0)
            flameAreaLeft = newFlameAreaLeft;
    }

    public void setFlameAreaRight(int newFlameAreaRight) {
        if (newFlameAreaRight > 0)
            flameAreaRight = newFlameAreaRight;
    }

    public void setFlameAreaUp(int newFlameAreaUp) {
        if (newFlameAreaUp > 0)
            flameAreaUp = newFlameAreaUp;
    }

    public void setFlameAreaDown(int newFlameAreaDown) {
        if (newFlameAreaDown > 0)
            flameAreaDown = newFlameAreaDown;
    }

    public String getOwner() { return Owner; }
}
