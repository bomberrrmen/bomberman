package Bomberman;

public class Multiplayer {


    public static volatile boolean initDone = false;  // For synchronization
    private static Client client = null;
    private static Server server = null;
    private static boolean Network_Role;
    private static int Client_Num;

    public static void Run(String serverID, String clientID, boolean isServer, int Client_num)
    {
        if (isServer)
        {

        // The server computer creates a server and a client object:
        server = new Server(8888, Client_num, serverID);
        // Starting the server and connecting the client:
        server.Start();

        }

        client = new Client(clientID);
        client.Connect();

        Network_Role = isServer;
        Client_Num = Client_num;

        initDone = true;

        // Closing the client connection and the server:
        ////client.Close();
        //server.Close();

    }

    public static void Send(Multiplayer_Packet Packet)

    {
        if (client.isReady())
        {
            client.Send(Packet);
        }
        else
        {
            System.out.println("Client is not connected to the server!");
        }
    }

    public static String getClientID()
    {
        return client.getID();
    }

    public static boolean getIsServer()
    {
        return Network_Role;
    }

    public static int getClients()
    {
        if (server != null)
        {
            return server.getClients();
        }
        else
        {
            return 0;
        }
    }

    public static int getClient_Num() {return Client_Num; }

    public static void Stop_Broadcast()
    {
        if (server != null)
        {
            server.KillBroadcast();
        }

    }
}

