package Bomberman;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

public class Reward extends JComponent {

    private String Type;
    private int X;
    private int Y;
    private int Height = 40;
    private int Width = 40;
    private BufferedImage Image;


    /*public Reward()
    {
        Type = "NOTHING";
        X = 0;
        Y = 0;
    }*/

    public Reward(String Type, int X, int Y)
    {
        this.Type = TypeCheck(Type);
        this.X = X;
        this.Y = Y;
    }




    public void Paint(Graphics g, boolean gameOver)
    {
        if (!gameOver)
            g.drawImage(Image, X - Width/2, Y - Height / 2, this);
    }



    public String getType()
    {
        return Type;
    }

    public int getX()
    {
        return X;
    }

    public int getY()
    {
        return Y;
    }


    private String TypeCheck(String Type)
    {
        try {
            switch (Type) {
                case "SPEED":
                {
                    Image = ImageIO.read(new File(System.getProperty("user.dir") +
                            System.getProperty("file.separator") + "icons" +
                            System.getProperty("file.separator") + "Reward_Speed2.png"));
                    return Type;
                }

                case "BOMB":
                {
                    Image = ImageIO.read(new File(System.getProperty("user.dir") +
                            System.getProperty("file.separator") + "icons" +
                            System.getProperty("file.separator") + "Reward_Bomb2.png"));
                    return Type;
                }

                case "RANGE":
                {
                    Image = ImageIO.read(new File(System.getProperty("user.dir") +
                            System.getProperty("file.separator") + "icons" +
                            System.getProperty("file.separator") + "Reward_Range2.png"));
                    return Type;
                }

                case "HP":
                {
                    Image = ImageIO.read(new File(System.getProperty("user.dir") +
                            System.getProperty("file.separator") + "icons" +
                            System.getProperty("file.separator") + "Reward_HP2.png"));
                    return Type;
                }

                case "TIME":
                {
                    Image = ImageIO.read(new File(System.getProperty("user.dir") +
                            System.getProperty("file.separator") + "icons" +
                            System.getProperty("file.separator") + "Reward_Time2.png"));
                    return Type;
                }

                case "RANDOM":
                {
                    String[] Types = {"SPEED", "BOMB", "RANGE", "HP", "TIME"};
                    Random Random_Selector = new Random();
                    int Selected = Random_Selector.nextInt(Types.length);

                    switch (Selected)
                    {
                        case 0:
                        {
                            Image = ImageIO.read(new File(System.getProperty("user.dir") +
                                    System.getProperty("file.separator") + "icons" +
                                    System.getProperty("file.separator") + "Reward_Speed2.png"));
                            break;
                        }

                        case 1:
                        {
                            Image = ImageIO.read(new File(System.getProperty("user.dir") +
                                    System.getProperty("file.separator") + "icons" +
                                    System.getProperty("file.separator") + "Reward_Bomb2.png"));
                            break;
                        }

                        case 2:
                        {
                            Image = ImageIO.read(new File(System.getProperty("user.dir") +
                                    System.getProperty("file.separator") + "icons" +
                                    System.getProperty("file.separator") + "Reward_Range2.png"));
                            break;
                        }

                        case 3:
                        {
                            Image = ImageIO.read(new File(System.getProperty("user.dir") +
                                    System.getProperty("file.separator") + "icons" +
                                    System.getProperty("file.separator") + "Reward_HP2.png"));
                            break;
                        }

                        case 4:
                        {
                            Image = ImageIO.read(new File(System.getProperty("user.dir") +
                                    System.getProperty("file.separator") + "icons" +
                                    System.getProperty("file.separator") + "Reward_Time2.png"));
                            break;
                        }
                    }

                    return Types[Selected];
                }

                default:
                {
                    Image = ImageIO.read(new File(System.getProperty("user.dir") +
                            System.getProperty("file.separator") + "icons" +
                            System.getProperty("file.separator") + "Reward_Nothing2.png"));
                    return "NOTHING";
                }

            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return "NOTHING";
    }



}
